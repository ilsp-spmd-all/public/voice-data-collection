# Voice Data Collection

## How To View
1.  Navigate to `https://apps.ilsp.gr/planv/project_name`, for instance: `https://apps.ilsp.gr/planv/augmented-narrative`

## Local Deployment

For detailed instructions (in Greek) on how to deploy locally, click [here](how_to.md).
## Remote Deployment

### Frontend

You can either build it locally and send the front-end files to the server, or git clone the repository to your home folder in the server. In any case:

1.  Install npm
    1. curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.1/install.sh | bash
    2. export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
       [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
    3. nvm install node

2.  `cd frontend`
3.  `npm install`
4.  Go back to the root folder: `cd ..`
5.  `make reset`
6.  `make deploy` (change `deploy` to the Makefile item you want to deploy, e.g., `deploy-narrative`)
7.  `make build`
8.  if local build `rsync -azP frontend/demo/dist <username>@<IP>:~/`
    if vm build: `cp -r frontend/demo/dist ~/`
9.  on the server, create a project folder under `/var/www/apps/planv/`. For instance, if your project name is `project_name`:
    1. `sudo mkdir -p /var/www/apps/planv/project_name`
    2. `sudo mv ~/dist/* /var/www/apps/planv/project_name`


### Backend
Install and setup Nginx
https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-gunicorn-and-nginx-on-ubuntu-18-04

1. Copy the backend folder to your home page on the server
2. Copy the backend folder to a project folder under `/var/www/apps_backends/`. For instance, if your project name is `project_name`:
    `sudo mkdir -p /var/www/apps_backends/project_name && sudo cp -r backend /var/www/apps_backends/project_name`
3.  Go back to your home folder and create a virtual environment with the requirements:
    1. `cd ~/voice-data-collection/backend`
    2. `virtualenv -p python3 ~/planv_env`
    3. `source ~/planv_env/bin/activate`
    4. `pip install -r requirements.txt` 
4.  Create Gunicorn service:
    https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-gunicorn-and-nginx-on-ubuntu-18-04
    1.  Make sure to replace `project_name` with your project name: `sudo vim /etc/systemd/system/project_name.service`  
    2.  paste the following code, making sure to replace USERNAME with your own username and `project_name` with your project name (incl. project_name.sock):

            [Unit]
            Description=Gunicorn instance to serve planv
            After=network.target

            [Service]
            User=USERNAME
            Group=www-data
            WorkingDirectory=/var/www/apps_backends/project_name
            Environment="PATH=/home/USERNAME/planv_env/bin"
            ExecStart=/home/USERNAME/planv_env/bin/gunicorn --workers 1 --limit-request-line 0 --bind unix:project_name.sock -m 007 wsgi:app -k flask_sockets.worker

            [Install]
            WantedBy=multi-user.target

    3.  Autostart service on boot (replace `project_name`):
        `sudo systemctl enable project_name.service`

5.  Create Nginx config for the app:
    1. `sudo vim /etc/nginx/sites-available/planv_backend`
    2. paste the following code. If the rest already exist, only append the /project_name/ one (and make sure you use your own project name):

            server {
                    listen 5000 ssl;
                    listen [::]:5000 ssl;

                    server_name apps.ilsp.gr;
                    ssl_certificate /etc/letsencrypt/live/apps.ilsp.gr/fullchain.pem;
                    ssl_certificate_key /etc/letsencrypt/live/apps.ilsp.gr/privkey.pem;
                    include /etc/letsencrypt/options-ssl-nginx.conf;

                    location / {
                        include proxy_params;
                        proxy_pass http://unix:/var/www/apps/planv/backend/planv.sock;
                        proxy_http_version 1.1;
                        proxy_set_header Upgrade $http_upgrade;
                        proxy_set_header Connection 'upgrade';
                        proxy_set_header Host $host;
                        proxy_cache_bypass $http_upgrade;
                    }

                    location /project_name/ {
                        include proxy_params;
                        proxy_pass http://unix:/var/www/apps_backends/project_name/project_name.sock;
                        # Alow the use of websockets
                        proxy_http_version 1.1;
                        proxy_set_header Upgrade $http_upgrade;
                        proxy_set_header Connection 'upgrade';
                        proxy_set_header Host $host;
                        proxy_cache_bypass $http_upgrade;
                    }
            }

6.  To initialize the database make sure you are still within the virtualenv. If not, repeat step 3.3: `source ~/planv_env/bin/activate`. Then:
    1.  `cd /var/www/apps_backends/project_name/`
    2.  `python3`
        `from __init__ import db, create_app`
        `db.create_all(app=create_app())`

Now that the database has been created, we can use `curl` to create a user. From any terminal (not necessarily on the server) you can run:
7. `curl --location --request POST 'https://apps.ilsp.gr:5000/project_name/signup'  --form 'username=user'  --form 'password=pass'`

The app should be accessible at `https://apps.ilsp.gr/planv/project_name` and you should be able to access it with the credentials that you just created in step 7.
