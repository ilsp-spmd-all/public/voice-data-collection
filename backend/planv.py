from flask import Flask, request, Response, render_template
from flask_api import status

import json
import os
import gevent
import re
import base64
from gevent.pywsgi import WSGIServer
import time
from werkzeug.utils import secure_filename
from werkzeug.datastructures import  FileStorage
from flask_login import login_required, current_user
from flask_sockets import Sockets
from __init__ import db, create_app
import pathlib
#change project for other versions
# from projects.bdae_part_one.stories import stories
# from projects.bdae_part_three.stories import stories
# from projects.new_narrative.stories import stories
# from projects.dysarthria.stories import stories
from {{{BACKEND_PROJECT}}} import stories


# RECORDINGS_PATH = "/home/dimastro/planv_recordings"
RECORDINGS_PATH = "{{{RECORDINGS_DIR}}}"
# FRONTEND_HOST = "https://apps.ilsp.gr"
# FRONTEND_HOST = "http://localhost:3000"
FRONTEND_HOST = "{{{FRONTEND_URL}}}"

app = create_app()


sockets = Sockets(app)

audio = bytearray()
filename = ""

# Create recordings path if does not exist
pathlib.Path(RECORDINGS_PATH).mkdir(parents=True, exist_ok=True) 

# Check if all stories folders exist
# if not create empty folder 
for story in stories:
    pathlib.Path(story['path']).mkdir(parents=True, exist_ok=True) 

def load_json(myjson):
  try:
    json_object = json.loads(myjson)
  except ValueError as e:
    return False
  return json_object

@sockets.route('/')
@login_required
def stream_socket(ws):
    header_flag = False 
    while not ws.closed:
        message = ws.receive()
        message_json = load_json(message)
        
        if message_json and "control" in message_json:
            if message_json["control"] == "start":
                audio = bytearray()
                print("start sending")
                ws.send(json.dumps({"status": "accepting"}))
                filename = message_json["filename"]
            elif message_json["control"] == "header":
                header_flag = True  
            elif message_json["control"] == "ping":
                print("got ping!")
            elif message_json["control"] == "end":
                with open("{}/{}".format(RECORDINGS_PATH, filename), "wb") as file:
                    file.write(audio)

                print("end sending")
                ws.send(json.dumps({"status": "received"}))
            elif message_json["control"] == "close":
                ws.closed=True
                print("closed socket")
            
        elif not message == "undefined":
            if not header_flag:
                audio += message
                print("got chunk")
            else: 
                audio = message + audio
                header_flag = False
                print('got header')

@app.after_request
def after_request(response):
    """!
    @brief Add necessary info to headers. Useful for preventing CORS permission errors.
    """
    response.headers.add("Access-Control-Allow-Origin", FRONTEND_HOST)
    response.headers.add("Access-Control-Allow-Credentials", "true")
    response.headers.add("Access-Control-Allow-Headers", "Content-Type,Authorization")
    response.headers.add("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS")
    return response

@app.route('/save_audio', methods=['POST'])
@login_required
def save_audio():
    if request is None or \
       not request.files.getlist('audio') or \
       not isinstance(request.files.getlist('audio')[0], FileStorage):
        resp = {
            "error": "No JSON POSTed containing the wav"
        }
        return Response(
            resp,
            status=status.HTTP_400_BAD_REQUEST,
            mimetype="application/json")

    audio = request.files.getlist('audio')[0]
    # Sanitise the filename
    filename = secure_filename(audio.filename)
    # Save local copy of wav data
    if not os.path.exists(RECORDINGS_PATH):
        os.mkdir(RECORDINGS_PATH)

    rec_path = os.path.join(RECORDINGS_PATH, '{}.wav'.format(filename))

    audio.save(rec_path)
    response = 'Audiofile {} has been saved'.format(filename)

    return Response(
        response,
        status=status.HTTP_200_OK,
        mimetype="application/json")


@app.route('/get_stories', methods=['GET'])
@login_required
def get_stories():
    resp = json.dumps(stories)
    return Response(
        resp,
        status=status.HTTP_200_OK,
        mimetype="application/json")

@app.route('/download_instructions', methods=['GET', 'POST']) 
@login_required
def download_instructions():
    pdf_filepath = './static/test/instructions.pdf'
    with open(pdf_filepath, 'rb') as pdf_file: 
        base64_bytes = base64.b64encode(pdf_file.read())
        base64_string = base64_bytes.decode('utf-8')
        resp = json.dumps({'pdf': base64_string})
    return Response(
        resp,
        status=status.HTTP_200_OK,
        mimetype="application/json")

@app.route('/get_photos/<id>', methods=['GET'])
@login_required
def get_photos(id):
    story_dict = next((item for item in stories if item["id"] == int(id)), None)
    if not story_dict:
        resp = {
            "error": "Story doesn't exist"
        }
        resp = json.dumps(resp)
        return Response(
            resp,
            status=status.HTTP_400_BAD_REQUEST,
            mimetype="application/json")

    path = story_dict["path"]
    photos = []
    for file in [f for f in os.listdir(path) if f.endswith('.jpg')]:
        with open(os.path.join(path, file), "rb") as image_file:
            base64_bytes = base64.b64encode(image_file.read())
            base64_string = base64_bytes.decode('utf-8')
            photos.append({"id": os.path.splitext(file)[0], "src": base64_string })

    resp = json.dumps(photos)
    return Response(
        resp,
        status=status.HTTP_200_OK,
        mimetype="application/json")

@app.route('/get_instructions/<id>', methods=['GET'])
@login_required
def get_instructions(id):

    if int(id)==42000:
        story_dict = next((item for item in stories if item["id"] == 25), None)
    elif int(id)==42001:
        story_dict = next((item for item in stories if item["id"] == 48), None)
    else:
        story_dict = next((item for item in stories if item["id"] == int(id)), None)
    if not story_dict:
        resp = {
            "error": "Story doesn't exist"
        }
        resp = json.dumps(resp)
        return Response(
            resp,
            status=status.HTTP_400_BAD_REQUEST,
            mimetype="application/json")

    path = story_dict["path"]
    audios = []
    for file in [f for f in os.listdir(path) if f.endswith('.mp3') and f.startswith("instructions")]:
        with open(os.path.join(path, file), "rb") as audio_file:
            base64_bytes = base64.b64encode(audio_file.read())
            base64_string = base64_bytes.decode('utf-8')
            audios.append({"id": os.path.splitext(file)[0], "src": base64_string })

    resp = json.dumps(audios)
    return Response(
        resp,
        status=status.HTTP_200_OK,
        mimetype="application/json")

@app.route('/get_audio/<id>', methods=['GET'])
@login_required
def get_audio(id):
    story_dict = next((item for item in stories if item["id"] == int(id)), None)
    if not story_dict:
        resp = {
            "error": "Story doesn't exist"
        }
        resp = json.dumps(resp)
        return Response(
            resp,
            status=status.HTTP_400_BAD_REQUEST,
            mimetype="application/json")

    path = story_dict["path"]
    audios = []
    for file in [f for f in os.listdir(path) if f.endswith('.mp3') and f.startswith("story")]:
        with open(os.path.join(path, file), "rb") as audio_file:
            base64_bytes = base64.b64encode(audio_file.read())
            base64_string = base64_bytes.decode('utf-8')
            audios.append({"id": os.path.splitext(file)[0], "src": base64_string })

    resp = json.dumps(audios)
    return Response(
        resp,
        status=status.HTTP_200_OK,
        mimetype="application/json")

@app.route('/get_other_audio/<id>', methods=['GET'])
@login_required
def get_other_audio(id):
    story_dict = next((item for item in stories if item["id"] == int(id)), None)
    if not story_dict:
        resp = {
            "error": "Story doesn't exist"
        }
        resp = json.dumps(resp)
        return Response(
            resp,
            status=status.HTTP_400_BAD_REQUEST,
            mimetype="application/json")

    path = story_dict["path"]
    audios = []
    # for file in [f for f in os.listdir(path) if f.endswith('.wav') ]:
    #     with open(os.path.join(path, file), "rb") as audio_file:
    #         base64_bytes = base64.b64encode(audio_file.read())
    #         base64_string = base64_bytes.decode('utf-8')
    #         audios.append({"id": os.path.splitext(file)[0], "src": base64_string })
    
    for file in [f for f in os.listdir(path) if f.endswith('.wav')]:
        with open(os.path.join(path, file), "rb") as audio_file:
            base64_bytes = base64.b64encode(audio_file.read())
            base64_string = base64_bytes.decode('utf-8')
            audios.append({"id": os.path.splitext(file)[0], "src": base64_string })

    resp = json.dumps(audios)
    return Response(
        resp,
        status=status.HTTP_200_OK,
        mimetype="application/json")


@app.route('/get_recording/<filename>', methods=['GET'])
@login_required
def get_recording(filename):
    print(filename)
    audiofile = filename + '.wav'
    audios = []
    with open(os.path.join(RECORDINGS_PATH, audiofile), "rb") as audio_file:
        base64_bytes = base64.b64encode(audio_file.read())
        base64_string = base64_bytes.decode('utf-8')
        audios.append({"id": os.path.splitext(audiofile)[0], "src": base64_string })

    resp = json.dumps(audios)
    return Response(
        resp,
        status=status.HTTP_200_OK,
        mimetype="application/json")

print(app.url_map)
