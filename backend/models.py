from __init__ import db, login_manager, login_serializer, cookie_duration

@login_manager.user_loader
def user_loader(user_id):
    return User.query.get(user_id)


@login_manager.request_loader
def load_token(request):
    """
    Flask-Login token_loader callback.
    The token_loader function asks this function to take the token that was
    stored on the users computer process it to check if its valid and then
    return a User Object if its valid or None if its not valid.
    """

    #The Token itself was generated by User.get_auth_token.  So it is up to
    #us to known the format of the token data itself.

    #The Token was encrypted using itsdangerous.URLSafeTimedSerializer which
    #allows us to have a max_age on the token itself.  When the cookie is stored
    #on the users computer it also has a exipry date, but could be changed by
    #the user, so this feature allows us to enforce the exipry date of the token
    #server side and not rely on the users cookie to exipre.
    max_age = cookie_duration

    #Decrypt the Security Token, data = [username, hashpass]
    token = request.args.get('token')
    if not token:
        return None
    data = login_serializer.loads(token, max_age=max_age)

    #Find the User
    user = User.query.get(data[0])

    #Check Password and return user or None
    if user and data[1] == user.password:
        return user
    return None


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True) # primary keys are required by SQLAlchemy
    username = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))
    is_active = db.Column(db.Boolean)
    is_authenticated = db.Column(db.Boolean)
    def to_json(self):
        return {"username": self.username}

    def is_authenticated(self):
        return self.is_authenticated

    def is_active(self):
        return self.is_active

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)

    def get_auth_token(self):
        """
        Encode a secure token for cookie
        """
        data = [str(self.id), self.password]
        return login_serializer.dumps(data)
