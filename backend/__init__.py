from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from itsdangerous import URLSafeTimedSerializer
from config import Config

# init SQLAlchemy so we can use it later in our models
db = SQLAlchemy()
login_manager = LoginManager()
config = Config()
login_serializer = URLSafeTimedSerializer(config.SECRET_KEY)
cookie_duration = config.REMEMBER_COOKIE_DURATION.total_seconds()

def create_app():
    app = Flask(__name__)

    app.config.from_object('config.Config')

    ## Using a production configuration
    # app.config.from_object('config.ProdConfig')

    # Using a development configuration
    app.config.from_object('config.DevConfig')

    db.init_app(app)

    login_manager.init_app(app)
    login_manager.login_view = 'auth.login'

    # blueprint for auth routes in our app
    from auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    return app
