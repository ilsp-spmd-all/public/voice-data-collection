echo "id, username, password" > users.csv
for p in $(seq 1 100);
do
  pass=$(openssl rand -base64 48 | tr -d "=+/" | cut -c1-10)
  curl -X POST -H 'Content-Type: application/x-www-form-urlencoded, ' -d "username=user$p&password=$pass" https://apps.ilsp.gr:5000/signup
  id=$(curl -X POST -H 'Content-Type: application/x-www-form-urlencoded, ' -d "username=user$p&password=$pass" https://apps.ilsp.gr:5000/getid)
  echo "$id, user$p, $pass" >> users.csv
done
