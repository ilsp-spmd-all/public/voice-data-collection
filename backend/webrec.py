from flask import Flask, request, Response, render_template
from flask_api import status

import json
import os
import gevent
import re
from gevent.pywsgi import WSGIServer
import time
from werkzeug.utils import secure_filename
from werkzeug.datastructures import  FileStorage

app = Flask(__name__)
RECORDINGS_PATH = "/home/dimastro/planv_recordings"
FRONTEND_HOST = "https://apps.ilsp.gr"

@app.after_request
def after_request(response):
    """!
    @brief Add necessary info to headers. Useful for preventing CORS permission errors.
    """
    response.headers.add("Access-Control-Allow-Origin", FRONTEND_HOST)
    response.headers.add("Access-Control-Allow-Credentials", "true")
    response.headers.add("Access-Control-Allow-Headers", "Content-Type,Authorization")
    response.headers.add("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS")
    return response

@app.route('/save_audio', methods=['POST'])
def save_audio():
    if request is None or \
       not request.files.getlist('audio') or \
       not isinstance(request.files.getlist('audio')[0], FileStorage):
        resp = {
            "error": "No JSON POSTed containing the wav"
        }
        return Response(
            resp,
            status=status.HTTP_400_BAD_REQUEST,
            mimetype="application/json")

    audio = request.files.getlist('audio')[0]
    # Sanitise the filename
    filename = secure_filename(audio.filename)
    # Save local copy of wav data
    if not os.path.exists(RECORDINGS_PATH):
        os.mkdir(RECORDINGS_PATH)

    rec_path = os.path.join(RECORDINGS_PATH, '{}.wav'.format(filename))

    audio.save(rec_path)
    # save_wav(audio, rec_path)
    response = 'Audiofile {} has been saved'.format(filename)

    return Response(
        response,
        status=status.HTTP_200_OK,
        mimetype="application/json")


# @app.route('/save_file_s3/<filename>/<bucket>', methods=['GET'])
# def save_file_s3(filename, bucket):
#     if not bucket:
#         bucket = 'webrec'
#     filename = '{}.wav'.format(filename)
#     filepath = '{}/{}'.format(RECORDINGS_PATH, filename)
#     is_file = os.path.isfile(filepath)
#     if not filename or not is_file:
#         resp = {"error": "No filename"}
#
#         return Response(
#             resp,
#             status=status.HTTP_400_BAD_REQUEST,
#             mimetype="application/json")
#
#     s3 = boto3.resource('s3')
#     s3.Object(bucket, filename).put(Body=open(filepath, 'rb'))
#     os.remove(filepath)
#     resp = '{} has been saved'.format(filename)
#
#     return Response(
#         resp,
#         status=status.HTTP_200_OK,
#         mimetype="application/json")


@app.route('/get_completed_sentences/<username>/<device>/<behaviors_len>', methods=['GET'])
def get_sentences(username, device, behaviors_len):
    # Delete "_" from username
    username = username.replace("_", "")
    print(username, device)
    if not username or not device:
        resp = {"error": "No username or device"}

        return Response(
            resp,
            status=status.HTTP_400_BAD_REQUEST,
            mimetype="application/json")

    users_files = []
    # s3 = boto3.resource('s3')
    # my_bucket = s3.Bucket('webrec')
    # #test_bucket = my_bucket.objects #.filter(Prefix='foo/bar')
    # for object in my_bucket.objects.all():
    #     filename = object.key.split(".")
    #     filename_p = filename[0].split("_")
    #     if username == filename_p[0] and device == filename_p[2]:
    #         users_files.append(filename_p)
    # files_dict = {}
    # for elem in users_files:
    #     if elem[1] not in files_dict:
    #         files_dict[elem[1]] = []
    #     files_dict[elem[1]].append(elem[3])

    completed_sentences = []
    # for key in files_dict:
    #     if len(files_dict[key]) >= int(behaviors_len):
    #         completed_sentences.append(key)

    resp = json.dumps(completed_sentences)
    return Response(
        resp,
        status=status.HTTP_200_OK,
        mimetype="application/json")

