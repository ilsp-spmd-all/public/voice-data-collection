# GLOBAL VARIABLES
port_bdae_part_one=5001
port_bdae_part_three=5002
port_narrative=5004
port_dysarthria=5005
port_test=4999
port_local=5000
port_remote=5000

backend_url=https:\/\/apps.ilsp.gr:
websocket_url=wss:\/\/apps.ilsp.gr:
local_url=http:\/\/localhost:
local_websocket=ws:\/\/localhost:

project_name_bdae_part_one=bdae_part_one
project_name_bdae_part_three=bdae_part_three
project_name_narrative=new_narrative
project_name_dysarthria=dysarthria

# variable for wsgi.py
project_url_bdae_part_one=bdae-part-1
project_url_bdae_part_three=bdae-part-3
project_url_narrative=augmented-narrative
project_url_dysarthria=dysarthria

# variables for planv.py
frontend_url_local=http:\/\/localhost:3000
frontend_url=https:\/\/apps.ilsp.gr

recordings_dir_local=.\/recordings
recordings_dir_remote=\/var\/www\/recordings\/

#title variables for App.js
title_bdae=Συλλογή δεδομένων αφασικού λόγου
subtitle_bdae_part_one= Πρωτόκολλο BDAE (Μέρος Ι)
subtitle_bdae_part_three=PLan-V: Πρωτόκολλο BDAE (Μέρος ΙΙΙ)
title_dysarthria=Συλλογή δεδομένων δυσαρθρικού λόγου
title_narrative=Συλλογή δεδομένων αφηγηματικού λόγου
subtitle_planv=PLan-V

version=2

reset:
	cp frontend/utils/config.js.bu frontend/utils/config.js
	cp frontend/demo/src/App.js.bu frontend/demo/src/App.js
	cp frontend/nwb.config.js.bu frontend/nwb.config.js
	cp backend/planv.py.bu backend/planv.py
	cp backend/wsgi.py.bu backend/wsgi.py

build:
	cd ./frontend && npm run build

deploy:
	sed -i.bak 's/{{{BACKEND_URL}}}/${backend_url}/g' frontend/utils/config.js
	sed -i.bak 's/{{{WEBSOCKET_URL}}}/${websocket_url}/g' frontend/utils/config.js
	sed -i.bak 's/{{{VERSION}}}/${version}/g' frontend/utils/config.js

deploy-test:
	sed -i.bak 's/{{{BACKEND_URL}}}/${backend_url}${port_test}/g' frontend/utils/config.js
	sed -i.bak 's/{{{WEBSOCKET_URL}}}/${websocket_url}${port_test}/g' frontend/utils/config.js
	sed -i.bak 's/{{{VERSION}}}/${version}/g' frontend/utils/config.js


local-deploy:
	sed -i.bak 's/{{{BACKEND_URL}}}/${local_url}/g' frontend/utils/config.js
	sed -i.bak 's/{{{WEBSOCKET_URL}}}/${local_websocket}/g' frontend/utils/config.js
	sed -i.bak 's/{{{VERSION}}}/${version}/g' frontend/utils/config.js
	sed -i.bak 's/{{{PROJECT_URL}}}/""/g' backend/wsgi.py


deploy-bdae-part-one:
	sed -i.bak 's/{{{BACKEND_URL}}}/${backend_url}${port_remote}\/${project_url_bdae_part_one}/g' frontend/utils/config.js
	sed -i.bak 's/{{{WEBSOCKET_URL}}}/${websocket_url}${port_remote}\/${project_url_bdae_part_one}/g' frontend/utils/config.js
	sed -i.bak 's/{{{PROJECT_URL}}}/$(project_url_bdae_part_one)/g' frontend/demo/src/App.js
	sed -i.bak 's/{{{UI_TYPE}}}/images/g' frontend/demo/src/App.js
	sed -i.bak 's/{{{APP_TITLE}}}/${title_bdae}/g' frontend/demo/src/App.js
	sed -i.bak 's/{{{APP_SUBTITLE}}}/${subtitle_bdae_part_one}/g' frontend/demo/src/App.js
	sed -i.bak 's/{{{PATH}}}/$(project_name_bdae_part_one)/g' frontend/nwb.config.js

	sed -i.bak 's/{{{BACKEND_PROJECT}}}/projects.$(project_name_bdae_part_one).stories/g' backend/planv.py
	sed -i.bak 's/{{{FRONTEND_URL}}}/${frontend_url}/g' backend/planv.py
	sed -i.bak 's/{{{RECORDINGS_DIR}}}/${recordings_dir_remote}${project_name_bdae_part_one}/g' backend/planv.py

	sed -i.bak 's/{{{PROJECT_URL}}}/"\/${project_url_bdae_part_one}"/g' backend/wsgi.py

	sed -i.bak 's/{{{VERSION}}}/${version}/g' frontend/utils/config.js

deploy-bdae-part-one-local:
	sed -i.bak 's/{{{BACKEND_URL}}}/${local_url}${port_local}/g' frontend/utils/config.js
	sed -i.bak 's/{{{WEBSOCKET_URL}}}/${local_websocket}${port_local}/g' frontend/utils/config.js
	sed -i.bak 's/{{{PROJECT_URL}}}/$(project_url_bdae_part_one)/g' frontend/demo/src/App.js
	sed -i.bak 's/{{{UI_TYPE}}}/images/g' frontend/demo/src/App.js
	sed -i.bak 's/{{{APP_TITLE}}}/${title_bdae}/g' frontend/demo/src/App.js
	sed -i.bak 's/{{{APP_SUBTITLE}}}/${subtitle_bdae_part_one}/g' frontend/demo/src/App.js
	sed -i.bak 's/{{{PATH}}}/$(project_name_bdae_part_one)/g' frontend/nwb.config.js

	sed -i.bak 's/{{{BACKEND_PROJECT}}}/projects.$(project_name_bdae_part_one).stories/g' backend/planv.py
	sed -i.bak 's/{{{FRONTEND_URL}}}/${frontend_url_local}/g' backend/planv.py
	sed -i.bak 's/{{{RECORDINGS_DIR}}}/${recordings_dir_local}/g' backend/planv.py

	sed -i.bak 's/{{{PROJECT_URL}}}/""/g' backend/wsgi.py

	sed -i.bak 's/{{{VERSION}}}/${version}/g' frontend/utils/config.js

deploy-narrative:
	sed -i.bak 's/{{{BACKEND_URL}}}/${backend_url}${port_remote}\/${project_url_narrative}/g' frontend/utils/config.js
	sed -i.bak 's/{{{WEBSOCKET_URL}}}/${websocket_url}${port_remote}\/${project_url_narrative}/g' frontend/utils/config.js
	sed -i.bak 's/{{{PROJECT_URL}}}/$(project_url_narrative)/g' frontend/demo/src/App.js
	sed -i.bak 's/{{{UI_TYPE}}}/images/g' frontend/demo/src/App.js
	sed -i.bak 's/{{{APP_TITLE}}}/${title_narrative}/g' frontend/demo/src/App.js
	sed -i.bak 's/{{{APP_SUBTITLE}}}/${subtitle_planv}/g' frontend/demo/src/App.js
	sed -i.bak 's/{{{PATH}}}/$(project_name_narrative)/g' frontend/nwb.config.js

	sed -i.bak 's/{{{BACKEND_PROJECT}}}/projects.$(project_name_narrative).stories/g' backend/planv.py
	sed -i.bak 's/{{{FRONTEND_URL}}}/${frontend_url}/g' backend/planv.py
	sed -i.bak 's/{{{RECORDINGS_DIR}}}/${recordings_dir_remote}${project_name_narrative}/g' backend/planv.py

	sed -i.bak 's/{{{PROJECT_URL}}}/"\/${project_url_narrative}"/g' backend/wsgi.py

	sed -i.bak 's/{{{VERSION}}}/${version}/g' frontend/utils/config.js

deploy-narrative-local:
	sed -i.bak 's/{{{BACKEND_URL}}}/${local_url}${port_local}/g' frontend/utils/config.js
	sed -i.bak 's/{{{WEBSOCKET_URL}}}/${local_websocket}${port_local}/g' frontend/utils/config.js
	sed -i.bak 's/{{{PROJECT_URL}}}/$(project_url_narrative)/g' frontend/demo/src/App.js
	sed -i.bak 's/{{{UI_TYPE}}}/images/g' frontend/demo/src/App.js
	sed -i.bak 's/{{{APP_TITLE}}}/${title_narrative}/g' frontend/demo/src/App.js
	sed -i.bak 's/{{{APP_SUBTITLE}}}/${subtitle_planv}/g' frontend/demo/src/App.js
	sed -i.bak 's/{{{PATH}}}/$(project_name_narrative)/g' frontend/nwb.config.js

	sed -i.bak 's/{{{BACKEND_PROJECT}}}/projects.$(project_name_narrative).stories/g' backend/planv.py
	sed -i.bak 's/{{{FRONTEND_URL}}}/${frontend_url_local}/g' backend/planv.py
	sed -i.bak 's/{{{RECORDINGS_DIR}}}/${recordings_dir_local}/g' backend/planv.py

	sed -i.bak 's/{{{PROJECT_URL}}}/""/g' backend/wsgi.py

	sed -i.bak 's/{{{VERSION}}}/${version}/g' frontend/utils/config.js

deploy-bdae-part-three:
	sed -i.bak 's/{{{BACKEND_URL}}}/${backend_url}${port_remote}\/${project_url_bdae_part_three}/g' frontend/utils/config.js
	sed -i.bak 's/{{{WEBSOCKET_URL}}}/${websocket_url}${port_remote}\/${project_url_bdae_part_three}/g' frontend/utils/config.js
	sed -i.bak 's/{{{PROJECT_URL}}}/$(project_url_bdae_part_three)/g' frontend/demo/src/App.js
	sed -i.bak 's/{{{UI_TYPE}}}/images/g' frontend/demo/src/App.js
	sed -i.bak 's/{{{APP_TITLE}}}/${title_bdae}/g' frontend/demo/src/App.js
	sed -i.bak 's/{{{APP_SUBTITLE}}}/${subtitle_bdae_part_three}/g' frontend/demo/src/App.js
	sed -i.bak 's/{{{PATH}}}/$(project_name_bdae_part_three)/g' frontend/nwb.config.js

	sed -i.bak 's/{{{BACKEND_PROJECT}}}/projects.$(project_name_bdae_part_three).stories/g' backend/planv.py
	sed -i.bak 's/{{{FRONTEND_URL}}}/${frontend_url}/g' backend/planv.py
	sed -i.bak 's/{{{RECORDINGS_DIR}}}/${recordings_dir_remote}${project_name_bdae_part_three}/g' backend/planv.py

	sed -i.bak 's/{{{PROJECT_URL}}}/"\/${project_url_bdae_part_three}"/g' backend/wsgi.py

	sed -i.bak 's/{{{VERSION}}}/${version}/g' frontend/utils/config.js

deploy-bdae-part-three-local:
	sed -i.bak 's/{{{BACKEND_URL}}}/${local_url}${port_local}/g' frontend/utils/config.js
	sed -i.bak 's/{{{WEBSOCKET_URL}}}/${local_websocket}${port_local}/g' frontend/utils/config.js
	sed -i.bak 's/{{{PROJECT_URL}}}/$(project_url_bdae_part_three)/g' frontend/demo/src/App.js
	sed -i.bak 's/{{{UI_TYPE}}}/images/g' frontend/demo/src/App.js
	sed -i.bak 's/{{{APP_TITLE}}}/${title_bdae}/g' frontend/demo/src/App.js
	sed -i.bak 's/{{{APP_SUBTITLE}}}/${subtitle_bdae_part_three}/g' frontend/demo/src/App.js
	sed -i.bak 's/{{{PATH}}}/$(project_name_bdae_part_three)/g' frontend/nwb.config.js

	sed -i.bak 's/{{{BACKEND_PROJECT}}}/projects.$(project_name_bdae_part_three).stories/g' backend/planv.py
	sed -i.bak 's/{{{FRONTEND_URL}}}/${frontend_url_local}/g' backend/planv.py
	sed -i.bak 's/{{{RECORDINGS_DIR}}}/${recordings_dir_local}/g' backend/planv.py

	sed -i.bak 's/{{{PROJECT_URL}}}/""/g' backend/wsgi.py

	sed -i.bak 's/{{{VERSION}}}/${version}/g' frontend/utils/config.js

deploy-dysarthria:
	sed -i.bak 's/{{{BACKEND_URL}}}/${backend_url}${port_dysarthria}/g' frontend/utils/config.js
	sed -i.bak 's/{{{WEBSOCKET_URL}}}/${websocket_url}${port_dysarthria}/g' frontend/utils/config.js
	sed -i.bak 's/{{{PROJECT_URL}}}/$(project_url_dysarthria)/g' frontend/demo/src/App.js
	sed -i.bak 's/{{{UI_TYPE}}}/words/g' frontend/demo/src/App.js
	sed -i.bak 's/{{{APP_TITLE}}}/${title_dysarthria}/g' frontend/demo/src/App.js
	sed -i.bak 's/{{{APP_SUBTITLE}}}/${subtitle_planv}/g' frontend/demo/src/App.js
	sed -i.bak 's/{{{PATH}}}/$(project_name_dysarthria)/g' frontend/nwb.config.js

	sed -i.bak 's/{{{BACKEND_PROJECT}}}/projects.$(project_name_dysarthria).stories/g' backend/planv.py
	sed -i.bak 's/{{{FRONTEND_URL}}}/${frontend_url}/g' backend/planv.py
	sed -i.bak 's/{{{RECORDINGS_DIR}}}/${recordings_dir_remote}${project_name_dysarthria}/g' backend/planv.py

	sed -i.bak 's/{{{PROJECT_URL}}}/"\/${project_url_dysarthria}"/g' backend/wsgi.py

	sed -i.bak 's/{{{VERSION}}}/${version}/g' frontend/utils/config.js

deploy-dysarthria-local:
	sed -i.bak 's/{{{BACKEND_URL}}}/${local_url}${port_local}/g' frontend/utils/config.js
	sed -i.bak 's/{{{WEBSOCKET_URL}}}/${local_websocket}${port_local}/g' frontend/utils/config.js
	sed -i.bak 's/{{{PROJECT_URL}}}/$(project_url_dysarthria)/g' frontend/demo/src/App.js
	sed -i.bak 's/{{{UI_TYPE}}}/words/g' frontend/demo/src/App.js
	sed -i.bak 's/{{{APP_TITLE}}}/${title_dysarthria}/g' frontend/demo/src/App.js
	sed -i.bak 's/{{{APP_SUBTITLE}}}/${subtitle_planv}/g' frontend/demo/src/App.js
	sed -i.bak 's/{{{PATH}}}/$(project_name_dysarthria)/g' frontend/nwb.config.js

	sed -i.bak 's/{{{BACKEND_PROJECT}}}/projects.$(project_name_dysarthria).stories/g' backend/planv.py
	sed -i.bak 's/{{{FRONTEND_URL}}}/${frontend_url_local}/g' backend/planv.py
	sed -i.bak 's/{{{RECORDINGS_DIR}}}/${recordings_dir_local}/g' backend/planv.py

	sed -i.bak 's/{{{PROJECT_URL}}}/""/g' backend/wsgi.py

	sed -i.bak 's/{{{VERSION}}}/${version}/g' frontend/utils/config.js
