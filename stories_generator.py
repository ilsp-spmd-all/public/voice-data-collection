import os 
from shutil import copyfile

ZIPPED_DIR = '../data'

if __name__ == '__main__':
    # data_path = os.path.join(ZIPPED_DIR, os.listdir(ZIPPED_DIR)[0])
    data_path = ZIPPED_DIR

    wav_id = 1
    
    Target_list_path = os.path.join(data_path, 'Target List')
    A_list_path = os.path.join(data_path, 'A list')
    B_list_path = os.path.join(data_path, 'B list')
    C_list_path = os.path.join(data_path, 'C list')
    D_list_path = os.path.join(data_path, 'D list')

    Target_list = os.listdir(Target_list_path)
    Target_list.sort()
    Target_list = Target_list[12::]

    A_list = os.listdir(A_list_path)
    A_list.sort()
    A_list = A_list[13::]

    B_list = os.listdir(B_list_path)
    B_list.sort()
    B_list = B_list[10:-1]

    C_list = os.listdir(C_list_path)
    C_list.sort()
    C_list = C_list[26::]
  
    D_list = os.listdir(D_list_path)
    D_list.sort()
    D_list = D_list[18::]

    # initialize empty dictionary
    stories = [] 

    for t_item in Target_list:
        tmp_dict = {}
        tmp_item = t_item.split('_')
        item_id = tmp_item[0]
        item_quote = tmp_item[1].split('.')[0]
        item_title = item_quote
        item_path = "./static/" + t_item.split('.')[0]
        tmp_dict = {"id": wav_id, "quote": item_quote, "title": item_title, "path": item_path}
        wav_id += 1
        destination_folder = os.path.join('../static', t_item.split('.')[0])
        if not os.path.exists(destination_folder):
            os.mkdir(destination_folder)
        copyfile(os.path.join(Target_list_path, t_item), os.path.join(destination_folder, t_item))
        stories.append(tmp_dict)

    for a_item in A_list:
        tmp_dict = {}
        tmp_item = a_item.split('_')
        item_id = tmp_item[0]
        item_quote = tmp_item[1].split('.')[0]
        item_title = item_quote
        item_path = "./static/" + a_item.split('.')[0]
        tmp_dict = {"id": wav_id, "quote": item_quote, "title": item_title, "path": item_path}
        wav_id += 1
        destination_folder = os.path.join('../static', a_item.split('.')[0])
        if not os.path.exists(destination_folder):
            os.mkdir(destination_folder)
        copyfile(os.path.join(A_list_path, a_item), os.path.join(destination_folder, a_item))
        stories.append(tmp_dict)

    for a_item in B_list:
        tmp_dict = {}
        tmp_item = a_item.split('_')
        item_id = tmp_item[0]
        item_quote = tmp_item[1].split('.')[0]
        item_title = item_quote
        item_path = "./static/" + a_item.split('.')[0]
        tmp_dict = {"id": wav_id, "quote": item_quote, "title": item_title, "path": item_path}
        wav_id += 1
        destination_folder = os.path.join('../static', a_item.split('.')[0])
        if not os.path.exists(destination_folder):
            os.mkdir(destination_folder)
        copyfile(os.path.join(B_list_path, a_item), os.path.join(destination_folder, a_item))
        stories.append(tmp_dict)

    for a_item in C_list:
        tmp_dict = {}
        tmp_item = a_item.split('_')
        item_id = tmp_item[0]
        item_quote = tmp_item[1].split('.')[0]
        item_title = item_quote
        item_path = "./static/" + a_item.split('.')[0]
        tmp_dict = {"id": wav_id, "quote": item_quote, "title": item_title, "path": item_path}
        wav_id += 1
        destination_folder = os.path.join('../static', a_item.split('.')[0])
        if not os.path.exists(destination_folder):
            os.mkdir(destination_folder)
        copyfile(os.path.join(C_list_path, a_item), os.path.join(destination_folder, a_item))
        stories.append(tmp_dict)
    
    for a_item in D_list:
        tmp_dict = {}
        tmp_item = a_item.split('_')
        item_id = tmp_item[0]
        item_quote = tmp_item[1].split('.')[0]
        item_title = item_quote
        item_path = "./static/" + a_item.split('.')[0]
        tmp_dict = {"id": wav_id, "quote": item_quote, "title": item_title, "path": item_path}
        wav_id += 1
        destination_folder = os.path.join('../static', a_item.split('.')[0])
        if not os.path.exists(destination_folder):
            os.mkdir(destination_folder)
        copyfile(os.path.join(D_list_path, a_item), os.path.join(destination_folder, a_item))
        stories.append(tmp_dict)

    print(stories)
