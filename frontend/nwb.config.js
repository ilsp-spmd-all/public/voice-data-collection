const path = require('path');
const waveWorker = path.resolve(
            __dirname,
            'node_modules/opus-recorder/dist/waveWorker.min.js')
const publicPath = path.resolve(
            __dirname,
            'public')
const PATHS = {
    new_narrative: '/planv/augmented-narrative/',
    test: '/planv/test/',
    bdae_part_one: '/planv/bdae-part-1/',
    bdae_part_three: '/planv/bdae-part-3/',
    dysarthria: '/planv/dysarthria/'
};
module.exports = {
    type: 'react-component',
    npm: {
        esModules: true,
        umd: {
            global: 'ReactPageScroller',
            externals: {
                react: 'React'
            }
        }
    },
    webpack: {
      publicPath: PATHS.{{{PATH}}},
        copy: {
      options: {
        debug: true
      },
      patterns: [
        {from: waveWorker, to: publicPath}
      ]},
        extra : {
            module: {
                rules: [
                  {
                    test: /waveWorker\.min\.js$/,
                    use: [{ loader: 'file-loader' , 
                      options: {
                        postTransformPublicPath: (p) => "https://apps.ilsp.gr/" + PATHS.{{{PATH}}} + p,
                      }
                    }],
                  }
                ]
              }
        },
        html: {
            template: 'demo/src/index.html'
        },
    },
    
};
console.log(waveWorker);
