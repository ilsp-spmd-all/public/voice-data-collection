import React, { Component } from "react";
import 'antd/dist/antd.css';
import {
         Spin, Alert, ButtonToolbar, Button, Icon, notification, Layout, Typography, Card, Row, Col, message,
        } from 'antd';
import { AudioOutlined, SoundOutlined , RightCircleOutlined, PauseCircleOutlined, LogoutOutlined, NodeIndexOutlined} from '@ant-design/icons';
import Recorder from 'recorder-js';
import {
  getAudioStream, exportBuffer, uploadFile, createHeader, downsampleBuffer,
} from '../../utils/audio';
import { SentencesContext, sentences } from "./globals";
import Gallery from 'react-grid-gallery';
import {BACKEND_URL, WEBSOCKET_URL, VERSION} from '../../utils/config.js';
import Grid from "antd/lib/card/Grid";
const {Header, Footer, Sider, Content} = Layout; 
const {Title} = Typography;
import {CountdownCircleTimer} from 'react-countdown-circle-timer';
import {navigate} from "@reach/router";

function concatenate(ResultConstructor, ...arrays) {
  let totalLength = 0;
  arrays.map((arr) => {
    totalLength += arr.length;
    return true;
  });
  const result = new ResultConstructor(totalLength);
  let offset = 0;
  arrays.map((arr) => {
    result.set(arr, offset);
    offset += arr.length;
    return true;
  });
  return result;
}


class RecorderComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            stream: null,
            recording: false,
            recorder: null,
            listen: false,
            socketReady: false,
            loading: false,
            countdown_over: false,
        };
        this.saveFile = this.saveFile.bind(this);
        this.startRecord = this.startRecord.bind(this);
        this.stopRecord = this.stopRecord.bind(this);
        this.streamAudio = this.streamAudio.bind(this);
        this.sampleRate = 44100;
        this.audioBlob = null;
        this.chunk = null;
        this.webSocket = null;
        this.sampleRateFinal = 22050;
        this.APIsampleRate = 22050;
        this.channels = 1;
        this.images = []
        this.thumbnail_height = 300;
        this.thumbnail_width = 500;




        this.s3_bucket = 'webrec';
        this.instructions_audio = React.createRef(null);

    }

  async componentDidMount() {
    this.setupBeforeUnloadListener();
    if (VERSION==1){
      this.forceUpdate(); // this needs to change as we force update when component is mounted 
    } 
    // await this.openSocket();
  }


    setupBeforeUnloadListener() {
    window.addEventListener('beforeunload', (ev) => {
      ev.preventDefault();
      return this.unloadWebsocket();
    });
  }

    async unloadWebsocket() {
    // await this.sendStop();
    await this.closeSocket();
  }


    async saveFile() {
        this.setState({ loading: true });
        this.audioRef.removeAttribute('src');
        this.audioRef.load();
        await this.props.goToPage(this.context.currentPage);
        this.setState({ loading: false, listen: false });
        // this.setState({ loading: true });
        // let filename = `${this.context.user}_${this.context.sentence.id}_${this.context.session_ts}`;
        //
        // this.audioRef.removeAttribute('src');
        // this.audioRef.load();
        // uploadFile(this.audioBlob, filename, this.s3_bucket).then(async (status) => {
        //     if (status) {
        //         await this.props.goToPage(this.context.currentPage);
        //     }
        //     this.setState({ loading: false, listen: false });
        //     this.audioBlob = null;
        // });
    }

    async prepareAudioStream() {
        let stream;

        try {
            stream = await getAudioStream();
        } catch (error) {
            // Users browser doesn't support audio.
            // Add your handler here.
            console.log(error);
        }

        this.setState({ stream });
        return stream;
    }

    setPID(newpid) {
    this.setState({
      pid: newpid,
    });
  }



    submitTCP() {
    const parThis = this;
    return new Promise((resolve, reject) => {
      console.log('Requesting PID for live demo...');
      parThis.setPID('');
      var rest_url = `${BACKEND_URL}/get_pid?channels=${parThis.channels}`;
      const options = {
        method: 'POST',
        headers: {
          'X-Auth-Token': parThis.state.token,
        },
        cache: 'no-cache',
      };
      fetch(rest_url, options)
        .then(data => data.json())
        .then((response) => {
          if (response) {
            const pid = response.pid;
            if (pid > 0) {
              parThis.setPID(pid);
              console.log(`Init successful with pid ${pid}`);
              resolve(true);
            } else {
              notification.error({
                message: 'Could not retrieve Process ID from API!',
              });
              reject('error');
            }
          } else {
            notification.error({
              message: 'Could not read API response!',
            });
            reject('error');
          }
        })
        .catch((error) => {
          notification.error({
            message: 'Error on POST request',
            description: `error: ${error}`,
          });
          reject('error');
        });
      return false;
    });
  }


    async startRecord() {
				this.audioBlob = null;
				this.chunk = new Float32Array();
				if (this.audioRef) {
					this.audioRef.removeAttribute('src');
					this.audioRef.load();
				}
				// const stream = await this.prepareAudioStream();
				// const header = createHeader(this.APIsampleRate, 400000000);
				// this.webSocket.send(header);

				// this.audioContext = new (window.AudioContext || window.webkitAudioContext)();
				// this.sampleRate = this.audioContext.sampleRate;
				// const recorder = new Recorder(this.audioContext);
				// recorder.init(stream);

				// this.setState(
				// 	{
				// 		recorder,
				// 		recording: true,
				// 	},
				// 	() => {
				// 		recorder.start();
				// 		this.streamAudio();
				// 	},
				// );
        //


    try {
      // Get new PID and start sending data
      if (
        this.webSocket === null
        || this.webSocket.readyState === WebSocket.CLOSED
      ) {
        await this.openSocket();
      }
      // await this.submitTCP();

      this.audioBlob = null;
      this.chunk = new Float32Array();
      if (this.audioRef) {
        this.audioRef.removeAttribute('src');
        this.audioRef.load();
      }
      const stream = await this.prepareAudioStream();
      console.log("streammmm", stream);
      this.header = createHeader(this.APIsampleRate, 4000000);
      await this.sendStart();


      if (!this.audioContext) {
        this.audioContext = new (window.AudioContext || window.webkitAudioContext)();
      }
      this.sampleRate = this.audioContext.sampleRate;
      const recorder = new Recorder(this.audioContext);
      recorder.init(stream);

      this.setState(
        {
          recorder,
          recording: true,
          countdown_over: false,
        },
      );
    } catch (error) {
      console.log('ooops ', error);
    }


    }

    openSocket() {
    const parThis = this;
    return new Promise((resolve, reject) => {
      // Ensures only one connection is open at a time
      console.log('Opening websocket connection...');
      if (
        parThis.webSocket !== null
        && parThis.webSocket.readyState !== WebSocket.CLOSED
      ) {
        console.log('WebSocket is already opened.');
        resolve();
      }
      // Create a new instance of the websocket
      console.log("token:", parThis.context.token);
      parThis.webSocket = new WebSocket(
        // `ws://${parThis.state.apihostname}:8080/stream/${parThis.state.clientid}?X-Auth-Token=${parThis.state.token}`,
        `${WEBSOCKET_URL}/?token=${parThis.context.token}`,
      );

      /**
       * Binds functions to the listeners for the websocket.
       */
      parThis.webSocket.onopen = function openSock(event) {
        console.log(event);
        parThis.setState({ socketReady: true });
        resolve(true);
      };

      parThis.webSocket.onmessage = function socketMessage(event) {
        console.log(event.data);
        console.log(JSON.parse(event.data));
        const status = JSON.parse(event.data).status;
        if (status == 'accepting') {
          const interval = setInterval(() => {
            if (parThis.state.recorder) {
              parThis.counter = 0;
              parThis.webSocket.send(parThis.header);
              parThis.state.recorder.start();
              // Stream first segment and then start interval
              parThis.streamAudio();
              parThis.streamAudioHandle = setInterval(parThis.streamAudio, 250);
              clearInterval(interval);
            }
          }, 20);
        }
      };

      parThis.webSocket.onclose = function (event) {
        console.log('Connection closed');
      };
      parThis.webSocket.onerror = function (event) {
        reject(event);
      };
    });
  }

  sendStart() {
    const parThis = this;
    let filename = `${this.context.user}_${this.context.sentence.id}_${this.context.session_ts}.wav`;
    return new Promise((resolve, reject) => {
      try {
        console.log(
          `Sending start message to WS for pid ${parThis.state.pid}`,
        );
        parThis.webSocket.send(
          JSON.stringify({
            control: 'start',
            filename: filename,
            // channels: parThis.channels.toString(),
          }),
        );
        resolve(true);
      } catch (e) {
        reject(e);
      }
    });
  }

  async sendStop() {
    const parThis = this;
    return new Promise((resolve, reject) => {
      console.log('Sending stop message to WS');
      parThis.webSocket.send(JSON.stringify({ control: 'end' }));
    });
    await this.closeSocket();
  }

  closeSocket() {
    const parThis = this;
    return new Promise((resolve, reject) => {
      parThis.webSocket.close();
    });
  }

  timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  streamAudio() {
    // const [ stream ] =  this.state.stream;
    // if (stream) {
    if (this.state.recording === true) {
      this.getChunk();
    }
    // } else {
    //   // We need sendStop() in order to send all segments back
    //   this.sendStop();
    //   clearInterval(this.streamAudioHandle);
    // }
  }


  convertFloat32ToInt16(buffer) {
    let l = buffer.length;
    const buf = new Int16Array(l);
    while (l--) {
      buf[l] = Math.min(1, buffer[l]) * 0x7fff;
    }
    return buf.buffer;
  }

  async getChunk() {
    const { recorder } = this.state;
    if (recorder == null) {
      return;
    }
    const { buffer } = await recorder.stop();
    recorder.start();
    const downsampledBuffer = downsampleBuffer(
      buffer[0],
      this.sampleRate,
      this.APIsampleRate,
    );
    const convertedBuffer = this.convertFloat32ToInt16(downsampledBuffer);
    if (this.webSocket === null || this.webSocket.readyState === WebSocket.CLOSED) {
      this.initRecording();
      notification.error({
        message: 'WebSocket is closed',
      });
      return;
    }
    if (convertedBuffer.byteLength > 0) {
      this.webSocket.send(convertedBuffer);
      this.chunk = concatenate(Float32Array, this.chunk, buffer[0]);
    }
  }

  initRecording() {
    clearInterval(this.streamAudioHandle);
    this.setState({
      recorder: null,
      stream: null,
      recording: false,
    });
  }

  async stopRecord() {
    const { recorder, stream } = this.state;

    const { buffer } = await recorder.stop();
    this.chunk = concatenate(Float32Array, this.chunk, buffer[0]);
    this.audioBlob = exportBuffer(this.chunk, this.sampleRate);

    this.setState({ listen: true });
    this.audioRef.src = URL.createObjectURL(this.audioBlob);
    this.audioRef.load();
    if (stream) {
      stream.getTracks().forEach((track) => {
        track.stop();
        stream.removeTrack(track);
      });
    }

    this.sendStop();
    this.initRecording();
  }


    startStopAudio(audio) {
      console.log("aud", audio);
        if (audio.paused) {
            audio.play();
        }else {
            audio.pause();
            audio.currentTime = 0;
        }

    }


    render() {
        const { recording, stream, listen } = this.state;
        const quote = this.context.sentence ? this.context.sentence.quote : ''; 
        const sentence_id = this.context.sentence ? this.context.sentence.id : ''; 
        let photos = ''; 
        let instructions = ''; 
        let story_audio = ''; 
        this.images = [];
        if (this.context.photos && this.context.photos.length > 0) {
            let photos_sorted = this.context.photos.sort((a,b) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
          console.log(photos_sorted.length);
            photos_sorted.forEach( (photo) => {
                this.images.push({
                                    src: `data:image/png;base64,${photo.src}`,
                                    thumbnail: `data:image/png;base64,${photo.src}`,
                                    thumbnailWidth: 340,
                                    thumbnailHeight: 200,
                                    thumbnailCaption: photo.id
                            });
            });
        }

        if (this.context.instructions && this.context.instructions.length > 0) {
          let instructions_sorted = this.context.instructions.sort((a,b) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
          instructions = []; 
          instructions_sorted.forEach( (i) => {
              var audio = new Audio();
              audio.id = `${i.id}_audio`;
              audio.src = `data:audio/wav;base64,${i.src}`;
              window[`${i.id}_audio`] = audio;
              instructions.push(<SoundOutlined style={{ fontSize: '100px', color: '#4E4E4E' }}  onClick={ () => this.startStopAudio(window[`${i.id}_audio`]) }/>);
          });
      }

      if (this.context.story_audio && this.context.story_audio.length > 0) {
        let story_audio_sorted = this.context.story_audio.sort((a,b) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
        story_audio = []; 
        story_audio_sorted.forEach( (i) => {
            var audio = new Audio();
            audio.id = `${i.src}_story_audio`;
            audio.src = `data:audio/wav;base64,${i.src}`;
            window[`${i.src}_story_audio`] = audio;
            story_audio.push(<SoundOutlined style={{ fontSize: '750%', color: '#39ac73' }}  onClick={ () => this.startStopAudio(window[`${i.src}_story_audio`]) }/>);
        });
      }                  

      const error = () => {
        message.error('Πρέπει πρώτα να ολοκληρώσετε την ηχογράφηση');
      };

      const success = () => {
        message.success('Έχετε παύσει τη χορήγηση');
      };

        // Don't show record button if their browser doesn't support it.
        return (
            <Spin className="spin" spinning={this.state.loading}>
                <Layout style={{height:'100vh', marginTop:100}}>
                  <Header style={{background: '#ffffff', padding: 0, textAlign: 'center'}}>
                    <Title level={2} style={{color: '#595959'}}>Παρακαλώ πείτε τη λέξη όσο πιο φυσικά μπορείτε</Title>
                  </Header>
                  <Layout>
                    <Content style={{background: '#ffffff'}}>
                      <Row>
                        <Col span={8}>
                          <Card title={sentence_id} style={{marginLeft: 100, marginTop: 80, background: '#BBDCFE'}}>
                            <Title level={1}>{quote}</Title>
                          </Card>
                        </Col>
                        <Col span={8} style={{marginTop: 100, marginLeft: 20}}>{instructions}</Col>
                        <Col span={6} style={{marginTop: 70}}>
                          {(this.state.recording && !this.state.countdown_over)?
                            <CountdownCircleTimer
                              onComplete={() => {
                                // do your stuff here
                                this.setState({countdown_over:true})
                                return [false, 1500] // repeat animation in 1.5 seconds if true 
                                }
                              }
                              size={150}
                              isPlaying
                              duration={3}
                              colors={[
                                ['#004777', 0.4],
                                ['#F7B801', 0.4],
                                ['#A30000', 0.2],
                              ]}
                              style={{marginTop: 20, marginLeft: 500}}
                            >
                              {({remainingTime, animatedColor}) => (
                                <h1 style={{color:animatedColor}}> {remainingTime} </h1>
                              )
                              }
                            </CountdownCircleTimer>:null}
                        </Col>
                      </Row>

                      <LogoutOutlined 
                        onClick={()=> {window.location.reload()}}
                        style={{marginBottom: 200, marginLeft: 100, fontSize: '64px', color: '#FF5733'}} 
                      />
                      <PauseCircleOutlined
                        onClick={()=> {
                          (recording)? this.stopRecord() : success();
                        }}
                        style={{marginBottom: 200, marginLeft: 10, fontSize:'64px'}} 
                      />
                      <RightCircleOutlined 
                        onClick={() => {
                          (listen && !recording) ? this.saveFile(): error(); }}
                        style={(listen && !recording)?(
                          {fontSize: '64px', color: '#008000', marginTop: 200, marginLeft: 10}
                        ):(
                          {fontSize: '64px', marginTop: 200, marginLeft: 10}
                        )}
                      />
                    </Content>
                    <Sider width="800" style={{background: '#ffffff'}} >
                      <AudioOutlined size="large"
                        onClick={()=> {
                          recording ? this.stopRecord() : this.startRecord();
                        }}
                        style={recording ? (
                          {fontSize: '100px', color: '#FF5733', marginTop: 100}
                        ) : 
                        ( {fontSize: '100px', color: '#4E4E4E', marginTop: 100})
                        }
                      ></AudioOutlined>
                      {listen ? <audio style={{marginTop:10}} ref={(audioRef)=>{this.audioRef=audioRef;}} controls /> : null}
                      {(listen && !recording) ? 
                        <Button style={{marginLeft:20}} type="normal" size="large" onClick={this.saveFile}>
                          <Icon type="save" />
                          {this.context.availableStories.length > 0 ? 'Επόμενο': 'Τέλος'}
                        </Button> : null}
                    </Sider>
                  </Layout>
                </Layout>
            </Spin>
      )
  }
}
RecorderComponent.contextType = SentencesContext;

export default RecorderComponent;

