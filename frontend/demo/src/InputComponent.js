import ReactDOM from 'react-dom'
import React from "react";
import 'antd/dist/antd.css';
import {
         Spin, Form, Input, Tooltip, Select, Button,
        } from 'antd';
import {QuestionCircleOutlined, KeyOutlined} from '@ant-design/icons'
import { SentencesContext, sentences } from "./globals";
import { UserLogin } from "../../utils/utils";
import LogoEE from'../assets/ee.jpg';
import LogoEPANEK from'../assets/epanek.jpg';
import LogoESPA from'../assets/espa.jpg';


const { Option } = Select;

class InputComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false
        };
        this.nextPage = this.nextPage.bind(this);
    }

    nextPage() {
        this.props.inputNextPage(this.context.currentPage);
    }

    handleSubmit = async (values) => {
        
        
                console.log('Received values of form: ', values);
                this.setState({ loading: true });
                let userlogin = await UserLogin(values.username, values.password);
                let token = userlogin.token;
                if (userlogin) {
                    await this.props.setUser(values.username, token);
                    await this.props.inputNextPage(this.context.currentPage);
                }
                this.setState({ loading: false });
            }
    

    render() {
        
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 8 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 8,
                },
            },
        };

        return (
          <div>
              <Spin className="spin" spinning={this.state.loading}>
            <div className="planv-header">
                <img  className="logos" src={LogoEE}  height="150"/>
                <img  className="logos" src={LogoEPANEK}  height="100"/>
                <img  className="logos" src={LogoESPA}  height="150"/>
            </div>
            <div className="planv-title">
              {this.props.title}
            </div>
            <div className="planv-title subtitle">
              {this.props.subtitle}
            </div>
            <div className="planv-form">
              <Form {...formItemLayout} onFinish={this.handleSubmit}>
                  <Form.Item
                    label={(
                      <span>
                        Username&nbsp;
                        <Tooltip title="Your name">
                          
                          <QuestionCircleOutlined />
                        </Tooltip>
                      </span>
                    )}
                    name="username"
                    rules={[{ required: true, message: 'Please input your username!' }]}
                  >
                  <Input style={{ width: 200 }}/>
                  </Form.Item>
                  
                  <Form.Item
                    label={(
                      <span>
                        Password&nbsp;
                        <Tooltip title="Password">
                          {/* <Icon type="KeyOutlined" /> */}
                          <KeyOutlined />
                        </Tooltip>
                      </span>
                    )}
                    name="password"
                    rules={[{ required: true, message: 'Please input your password!' }]}
                  >
                  <Input.Password style={{ width: 200 }}/>
                  </Form.Item>
                  
                  
                  
                  <Form.Item {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">Submit</Button>
                  </Form.Item>
                </Form>
            </div>
              </Spin>
        </div>
        )
    }
}

InputComponent.contextType = SentencesContext;

export default InputComponent;

