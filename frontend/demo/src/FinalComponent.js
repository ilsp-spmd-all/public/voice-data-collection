import ReactDOM from 'react-dom'
import React from "react";
import 'antd/dist/antd.css';
import {
         Button,
        } from 'antd';


class FinalComponent extends React.Component {
    constructor(props) {
        super(props);
        this.nextPage = this.nextPage.bind(this);
    }
    async nextPage() {
        await this.props.goToPage(this.context.currentPage);
    }

    render() {
        return (
          <div>
            <div className="planv-end">
              Σας ευχαριστούμε για τη συμμετοχή σας.
            </div>
                  <div className="start-button">
                      <Button type="normal" size="large" onClick={this.nextPage}>Αρχική Σελίδα</Button>
                  </div>
        </div>
        )
    }
}


export default FinalComponent;

