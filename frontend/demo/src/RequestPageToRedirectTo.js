import ReactDOM from 'react-dom'
import React from "react";
import 'antd/dist/antd.css';
import {
         Spin, Form, Input, Tooltip, Select, Button,
        } from 'antd';
import {QuestionCircleOutlined, KeyOutlined} from '@ant-design/icons'
import { SentencesContext, sentences } from "./globals";
import { UserLogin } from "../../utils/utils";
import LogoEE from'../assets/ee.jpg';
import LogoEPANEK from'../assets/epanek.jpg';
import LogoESPA from'../assets/espa.jpg';


const { Option } = Select;

class RequestPageToRedirectTo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false
        };
        this.nextPage = this.nextPage.bind(this);
    }

    nextPage() {
        this.props.inputNextPage(this.context.currentPage);
    }

    handleSubmit = async (values) => {
       
      this.setState({ loading: true });
      await this.props.inputNextPage(parseInt(values.page) +1);
      this.setState({ loading: false });
           
    }

    render() {
        
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 8 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 8,
                },
            },
        };

        const options = [
          { value: 'chocolate', label: 'Chocolate' },
          { value: 'strawberry', label: 'Strawberry' },
          { value: 'vanilla', label: 'Vanilla' }
        ]

        

        return (
          <div>
              <Spin className="spin" spinning={this.state.loading}>
            <div className="planv-title">
              Καλώς ορίσατε! 
            </div>
            <div className="planv-title subtitle">
              Θα θέλατε να μεταβείτε σε κάποια συγκεκριμένη σελίδα; 
            </div>
            <div className="planv-form">
              <Form {...formItemLayout} onFinish={this.handleSubmit} initialValues={{page:1}}>
                  <Form.Item
                    label={(
                      <span>
                        Σελίδα&nbsp;
                        <Tooltip title="Επιλέξτε την ιστορία στην οποία θα θέλατε να μεταβείτε για να συνεχίσετε τη συνεδρία σας.">
                          
                          <QuestionCircleOutlined/>
                        </Tooltip>
                      </span>
                    )}
                    name="page"
                  >
                    <Select style={{ width: 150 }}>
                      {/* <Option value="1">Εγκεφαλικό</Option>
                      <Option value="2">Ομπρέλα</Option>
                      <Option value="3">Γάτα</Option>
                      <Option value="4">Σταχτοπούτα</Option>
                      <Option value="5">Πάρτυ</Option>
                      <Option value="6">Δαχτυλίδι</Option>
                      <Option value="7">Λαγός</Option> */}
                      {this.props.pageNames.map((page_name, index) => <Option value={index+1}> {`${index}.${page_name}`} </Option>) };
                    </Select>
                  </Form.Item>
                  
                  <Form.Item {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">Συνέχεια</Button>
                  </Form.Item>
                </Form>
            </div>
              </Spin>
        </div>
        )
    }
}

RequestPageToRedirectTo.contextType = SentencesContext;

export default RequestPageToRedirectTo;

