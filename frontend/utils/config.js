export const BACKEND_URL = '{{{BACKEND_URL}}}';
export const WEBSOCKET_URL = '{{{WEBSOCKET_URL}}}';
export const VERSION = {{{VERSION}}};
